﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CharacterSet
{
    public int setID;
    public Dictionary<string, string> char_audioSet;
	private List<string> characters = new List<string>();
	private List<string> audioUrls = new List<string>();

    public CharacterSet()
    {
        char_audioSet = new Dictionary<string, string>();

    }
	public List<string> getCharacters(){
		return characters;
	}
	public List<string> getAudioUrlList(){
		return audioUrls;
	}
	public string getAudioUrl (string character){
		return char_audioSet [character];
	}
	public void setCharacters(List<string> characters){
		this.characters = characters;
	}
	public void addCharacter (string character){
		characters.Add (character);
	}
	public void setAudioUrls(List<string> audioUrls){
		this.audioUrls = audioUrls;
	}
	public void addAudioUrl (string url){
		audioUrls.Add (url);
	}
}
