﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class UserPerformance {
	// serializable version for json utility
	public string trial;
	public string responseTime;//userReactTime;
	public string numOfRow;//userChoosenRow;
	public string numOfCol;//userChoosenCol;
	public string char1;//userChoosenChar;
	public string ans;//ansWord;
	public string outcome;//trialOutcome;
	public string firsttime;
	public string ansrow;
	public string anscol;

	// the round number in this game level
	/*
	private int trial;
	private float userReactTime;//userReactTime;
	private int userChoosenRow;//userChoosenRow;
	private int userChoosenCol;//userChoosenCol;
	private string userChoosenChar;//userChoosenChar;
	private string ansWord;//ansWord;
	private int trialOutcome;//trialOutcome;
	private int firstTime;
	private int ansRow;
	private int ansCol;
	*/
	// server only allow integer, 0=> choose inncorrect, false, 1=> choose correct = true


	//is it the first word? it used in the second game 0=> is not first clicked,  false , 1=> is the first clicked, true

	public string getTrial() {
		return trial;
	}

	public void setTrial(int trial) {
		this.trial = trial.ToString ();
	}

	public string getUserChoosenRow() {
		return numOfRow;
	}

	public void setUserChoosenRow(int userChoosenRow) {
		this.numOfRow = userChoosenRow.ToString ();
	}

	public string getUserChoosenCol() {
		return numOfCol;
	}

	public void setUserChoosenCol(int userChoosenCol) {
		this.numOfCol = userChoosenCol.ToString ();
	}

	public string getUserChoosenChar() {
		return char1;
	}

	public void setUserChoosenChar(string userChoosenChar) {
		this.char1 = userChoosenChar;
	}

	public string getTrialOutcome() {
		return outcome;
	}

	public void setTrialOutcome(int trialOutcome) {
		this.outcome = trialOutcome.ToString ();
	}

	public string getFirstTime() {
		return firsttime;
	}

	public void setFirstTime(int firstTime) {
		this.firsttime = firstTime.ToString ();
	}

	public string getAnsWord() {
		return ans;
	}

	public void setAnsWord(string ansWord) {
		this.ans = ansWord;
	}

	public string getAnsRow() {
		return ansrow;
	}

	public void setAnsRow(int ansRow) {
		this.ansrow = ansRow.ToString ();
	}

	public string getAnsCol() {
		return anscol;
	}

	public void setAnsCol(int ansCol) {
		this.anscol = ansCol.ToString ();
	}
	public string getUserReactTime(){
		return responseTime;
	}
	public void setUserReactTime(float userReactTime){
		this.responseTime = userReactTime.ToString ("F3");
	}

}

