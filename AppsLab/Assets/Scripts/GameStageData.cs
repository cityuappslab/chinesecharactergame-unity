﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStageData {
	private int stage;
	private int game1;
	private int game2;
	private int game3;
	private int game4;

	public GameStageData (int st, int g1, int g2, int g3, int g4){
		stage = st;
		game1 = g1;
		game2 = g2;
		game3 = g3;
		game4 = g4;
	}

	public int getStage () {return stage;}
	public int getGame1 () {return game1;}
	public int getGame2 () {return game2;}
	public int getGame3 () {return game3;}
	public int getGame4 () {return game4;}
}
