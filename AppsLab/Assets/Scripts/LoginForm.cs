﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.Networking;

public class LoginForm : MonoBehaviour {

	private ScreenManager sm;
	private LoginController controller;
	public string url= "http://chinesecharactergame.cloudapp.net/";

	private string sessionId;
	private Dictionary<string, string> headers = new Dictionary<string, string>();

	private bool error = false;
	private string username = "";
	WWWForm form;
	// Use this for initialization
	void Awake () {
		sm = FindObjectOfType<ScreenManager> ();
		controller = FindObjectOfType<LoginController> ();
	}

	void Start (){
		
	}

	public void submitForm (string email, string password, bool remember) {
		StartCoroutine(submitLoginForm(email, password, remember.ToString()));
	}

	IEnumerator submitLoginForm(string email, string password, string remember) {
		// Create a Web Form
		form = new WWWForm();
		form.AddField("email", email);
		form.AddField ("password", password);
		form.AddField ("remember", remember);
		form.AddField ("api", "");

		WWW www = !this.headers.ContainsKey("COOKIE") ? new WWW(this.url + "login",form) : new WWW(this.url + "login", form.data, this.headers);
		yield return www;
		if (!string.IsNullOrEmpty(www.error)) {
			print(www.error);
			onFail ();
		}
		else {
			print(www.text);
			this.GetSessionId(www.responseHeaders);
			var N = JSONNode.Parse (www.text);
			error = N ["error"].AsBool;
			string msg = N ["msg"].Value;
			if (error){
				onFail ();
				controller.showMessage (msg);
			} else {
				username = N["user"] ["name"].Value;
				onSuccess ();
			}
		}
		//www.Dispose ();
	}

	private void onSuccess (){
		
		sm.SendMessage ("loginResult", true);
		sm.AssignPlayerName (username);
		//print (username);
	}

	private void onFail (){
		sm.SendMessage ("loginResult", false);
	}

    public void LogoutRequest()
    {
        StartCoroutine(logout());
    }

    private IEnumerator logout()
    {
		WWW www = new WWW(url + "logout",form);
		//WWW www = !this.headers.ContainsKey("COOKIE") ? new WWW(this.url+"logout",new WWWForm()) : new WWW(this.url + "logout", new WWWForm().data, this.headers);
		yield return www;
        if (!string.IsNullOrEmpty(www.error))
        {
            print(www.error);
        }
        else
        {
            print(www.text);
			//this.GetSessionId(www.responseHeaders);
            var N = JSONNode.Parse(www.text);
            error = N["error"].AsBool;
            if (error)
            {
                //fail
            }
            else
            {
                // success
                sm.logout();
				headers = new Dictionary<string, string> ();
            }
        }
    }

	public void requestStageData (){
		StartCoroutine (stageRequest ());
	}

	private IEnumerator stageRequest()
	{

		WWW www = !this.headers.ContainsKey("COOKIE") ? new WWW(this.url+"stage") : new WWW(this.url + "stage", null, this.headers);

		yield return www;

		Debug.Log(www.text);
		var N = JSONNode.Parse (www.text);
		error = N ["error"].AsBool;
		if (error){
			onFail ();
		} else {
			int stage = N ["stage"].AsInt;
			int game1 = N ["currentLevel"] [0] ["level"].AsInt + 1;
			int game2 = N ["currentLevel"] [1] ["level"].AsInt + 1;
			int game3 = N ["currentLevel"] [2] ["level"].AsInt + 1;
			int game4 = N ["currentLevel"] [3] ["level"].AsInt + 1;
			GameStageData data = new GameStageData (stage, game1, game2, game3, game4);
			sm.setGameStageData (data);
		}
		//this.GetSessionId(www.responseHeaders);
	}

	public void registerRequest (string name , string email, string regCode, string password, string pwConfirm){
		StartCoroutine (register (name,email,regCode,password,pwConfirm));
	}

	private IEnumerator register (string name, string email, string regCode, string password, string pw_confirm){
		WWWForm regForm = new WWWForm ();
		regForm.AddField ("name", name);
		regForm.AddField ("email",email);
		regForm.AddField ("reg_code",regCode);
		regForm.AddField ("password",password);
		regForm.AddField ("password_confirmation",pw_confirm);
		regForm.AddField ("api", "");
		WWW www = new WWW (url + "register", regForm);
		yield return www;
		if (!string.IsNullOrEmpty(www.error)) {
			print(www.error);
		}
		else {
			print(www.text);
			var N = JSONNode.Parse (www.text);
			error = N ["error"].AsBool;
			string msg = N ["msg"].Value;
			if (error){
				//controller.showMessage (msg);
			} else {
				print ("success"); 
			}
		}
	}

	public void uploadRecords (string gameID, string levelID, string win,JSONObject jObj, string accuracy) {
		StartCoroutine (uploadGameRecords (gameID, levelID, win, jObj, accuracy));
	}

	private IEnumerator uploadGameRecords (string gameID, string levelID, string win,JSONObject jObj, string accuracy){
		if (headers.ContainsKey ("Content-Type")){
			headers["Content-Type"] = "application/json";
		} else
			headers.Add ("Content-Type","application/json");
		
		byte[] formByte = System.Text.Encoding.ASCII.GetBytes (jObj.Print ().ToCharArray ());

		print (jObj.Print (true)); 

		WWW www = new WWW (url + "records", formByte, this.headers);

		yield return www;
		if (!string.IsNullOrEmpty(www.error)) {
			print(www.error);
		}
		else {
			print(www.text);
			var N = JSONNode.Parse (www.text);
			error = N ["error"].AsBool;
			string msg = N ["msg"].Value;
			if (error){
			} else {
				print ("record submitted"); 
			}
		}
	}


	private void GetSessionId(Dictionary<string, string> responseHeaders)
	{

		foreach (KeyValuePair<string, string> header in responseHeaders)
		{

			//Debug.Log(string.Format("{0} : {1}", header.Key, header.Value));

			if (header.Key == "SET-COOKIE")
			{

				string[] cookies = header.Value.Split(';');
				for (int i = 0; i < cookies.Length; i++)
				{

					if (cookies[i].Split('=')[0] == "laravel_session" && !this.headers.ContainsKey("COOKIE"))
					{
						this.sessionId = cookies[i];
						this.headers.Add("COOKIE", this.sessionId);
						break;
					}
				}
			}
		}
	}
}